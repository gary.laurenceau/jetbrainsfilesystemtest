# Simple File System Library

## Introduction

This library emulates a file system and use a single file as storage, available through the API 'FileSystem', 
implemented by 'SimpleFileSystem' class. It supports these operations:
- File creation
- File deletion
- Directory creation
- Directory deletion (with recursive effect)
- Read a file  
- Write content into a file

File without any directory specified are placed into the root directory.

This API is thread-safe.

No external dependency libraries are needed.

## File system architecture

When instantiated for the first time, a `Header` is added to the file, and lists the nodes in the file system.
Each entry is represented by a `Node`. File contents are appended at the end of the file. If new entry is needed, and no space is 
available in the header, a new header is appended at the end of the file and so on.

The possible nodes with their column are listed here: 
- `File`: directory's hashcode, filename's hashcode, directory name length, filename length, content offset, content length, max available content length
- `Directory`: directory's hashcode, directory name length, offset
- `EmtpyBlock`: offset, length
- `NextHeader`: offset
- `Unassigned`

Directories are separated by "/". This is an invalid character for a directory name.

Files and directory names are limited to 255 characters maximum and minimum 1.

File system content example:
```
SIMPLE_FILE_SYSTEM_001
e:00000000639:00000000019:00000000000:00000000009:00000000639:00000000000:00000000010    // Empty Block
f:00000000000:FILE_HASH01:00000000000:00000000009:00000000658:00000000005:00000000010    // File block
f:00000000000:FILE_HASH02:00000000000:00000000046:00000000677:00000000000:00000000010    // File block
d:DIRE_HASH03:00000000005:00000000670:00000000000:00000000000:00000000000:00000000000    // Directory block
e:00000000759:00000000027:00000000000:00000000017:00000000759:00000000000:00000000010    // Empty block
f:00000000000:FILE_HASH04:00000000000:00000000017:00000000786:00000000000:00000000010    // File block
f:00000000000:FILE_HASH05:00000000000:00000000009:00000000813:00000000012:00000000012    // File block
n:00000000835                                                                            // Next header
test1.txt**********test2.txtLorem*****long-file-name-123456789123456789123456789.txt**********another_file.txt**********another_file2.txt**********another_file3.txt**********test1.txtHello world!
f:00000000000:FILE_HASH06:00000000000:00000000017:00000001451:00000000546:00000000546    // File block
u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000    // Unassigned block
...
u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000    // Unassigned block
n:00000000000                                                                            // Next header not set yet
another_file2.txtRandomContentR....Content                                               // Directories names and files content
```

### Drawbacks / improvements

When files or directories are deleted, they are replaced by empty block nodes. These nodes can be reused,
but no rearrangement / realignment method have been implemented. So the file system size can only grow.

Node entries could be improved. Right now the method to generate and identify a file / directory uses `hashcode` method,
which is subject to a lot of collisions. Another hash method could be used, like sha1.

No escape methods have been set up, so character '/' is forbidden in directory name.