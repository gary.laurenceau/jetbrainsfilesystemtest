package com.test.filesytem

import com.test.filesytem.data.DirectoryAlreadyExistsException
import com.test.filesytem.data.FileAlreadyExistsException
import com.test.filesytem.data.IllegalDirectoryNameException
import com.test.filesytem.data.IllegalFileNameException
import com.test.filesytem.data.InitializeFileSystemException
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.NoSuchDirectoryException
import com.test.filesytem.data.NoSuchFileException
import com.test.filesytem.data.Node.Companion.PATH_SEPARATOR
import com.test.filesytem.data.SimpleDirectory
import com.test.filesytem.data.SimpleFile
import java.io.File
import java.io.RandomAccessFile
import kotlin.math.max

class SimpleFileSystem(
    private val fs: File
): FileSystem {

    companion object {
        const val ACCESS_MODE = "rw"
        const val MAX_DIR_NAME_LENGTH = 255
        const val MAX_FILE_NAME_LENGTH = 255
    }

    init {
        // Create file if not exists
        if (!fs.exists()) {
            fs.createNewFile().let { created ->
                if (!created) throw InitializeFileSystemException
                Header.initFSHeader(fs)
            }
        }
        else verifyFileSystemIntegrity()
    }

    // API

    /**
     * Creates a new file in the file system if it does not already exist.
     * If `directory` is not provided, file is placed in the root.
     * Returns a `SimpleFile`
     */
    @Throws(FileAlreadyExistsException::class, IllegalDirectoryNameException::class)
    @Synchronized override fun newFile(name: String, directory: String): SimpleFile {
        val fileName = name.trim()
        fileName.checkFileNameOrThrow()
        val directoryName = directory.cleanDirectoryName()
        if (directoryName.isNotEmpty()) {
            NodeFinder.findDirectory(fs, directoryName) ?: throw NoSuchDirectoryException(directoryName)
        }
        if (NodeFinder.findFile(fs, fileName, directoryName) != null) throw FileAlreadyExistsException(fileName)
        Header.insertFile(fs, fileName, directoryName)

        return SimpleFile(fileName, directoryName)
    }

    /**
     * Creates a new directory in the file system if it does not already exist.
     * To create subdirectory use '/' as separator between the directories.
     * Returns a `SimpleDirectory`.
     */
    @Throws(IllegalDirectoryNameException::class)
    @Synchronized override fun newDirectory(path: String): SimpleDirectory {
        val directoryName = path.cleanDirectoryName()
        if (directoryName.isBlank()) throw IllegalDirectoryNameException(path)
        val directories = directoryName.split(PATH_SEPARATOR)
        if (directories.size > 1) {
            val parent = directories.take(directories.size - 1).joinToString(PATH_SEPARATOR)
            NodeFinder.findDirectory(fs, parent) ?: throw NoSuchDirectoryException(parent)
        }

        if (NodeFinder.findDirectory(fs, directoryName) != null) throw DirectoryAlreadyExistsException(directoryName)
        Header.insertDirectory(fs, directoryName)
        return SimpleDirectory(directoryName)
    }

    /**
     * Deletes a file if it is found.
     */
    @Throws(NoSuchFileException::class, MalformedQueryException::class)
    @Synchronized override fun deleteFile(file: SimpleFile) {
        val fileNode = NodeFinder.findFile(fs, file.name, file.directory) ?: throw NoSuchFileException()
        Header.removeFileNode(fs, fileNode)
    }

    /**
     * Deletes a directory if it is found.
     * Subdirectories and files contained in this directory are deleted.
     */
    @Throws(NoSuchDirectoryException::class, MalformedQueryException::class)
    @Synchronized override fun deleteDirectory(directory: SimpleDirectory) {
        val directoryNode = NodeFinder.findDirectory(fs, directory.path) ?: throw NoSuchDirectoryException()

        // Find subdirectories and delete them
        NodeFinder.findSubDirectoriesInDirectory(fs, directory.path)
            .forEach { subDirectoryNode -> Header.removeDirectoryNode(fs, subDirectoryNode) }

        // Find files and delete them
        NodeFinder.findFilesInDirectory(fs, directory.path)
            .forEach { fileNode -> Header.removeFileNode(fs, fileNode) }

        // Delete specified directory
        Header.removeDirectoryNode(fs, directoryNode)
    }

    /**
     * Returns a `String` corresponding to the content of the specified file.
     */
    @Synchronized override fun readFile(file: SimpleFile): String {
        val fileNode = NodeFinder.findFile(fs, file.name, file.directory) ?: throw NoSuchFileException(file.name)
        val capacity = fileNode.contentLength
        val buffer = ByteArray(capacity.toInt())
        val offset = fileNode.offsetContent
        RandomAccessFile(fs, ACCESS_MODE)
            .also {
                it.seek(offset)
                it.read(buffer, 0, fileNode.contentLength.toInt())
                it.close()
            }
        return buffer.decodeToString()
    }

    /**
     * Writes content into the specified file. File has to be created first otherwise an `NoSuchFileException` is thrown.
     */
    @Throws(NoSuchFileException::class, MalformedQueryException::class)
    @Synchronized override fun writeFile(file: SimpleFile, content: String) {
        val name = file.name
        val parent = file.directory
        val fileNode = NodeFinder.findFile(fs, name, parent) ?: throw NoSuchFileException(name)
        val lengthToWrite = content.length
        val offsetContent: Long

        if (lengthToWrite <= fileNode.maxContentLength) {
            offsetContent = fileNode.offset
            val newNode = fileNode.copy(
                offset = offsetContent,
                contentLength = lengthToWrite.toLong(),
                maxContentLength = max(lengthToWrite.toLong(), fileNode.maxContentLength)
            )
            NodeFinder.replaceNode(fs, fileNode, newNode)
        }
        else {
            val fileNode = NodeFinder.findFile(fs, file.name, file.directory) ?: throw NoSuchFileException()
            Header.removeFileNode(fs, fileNode)
            val newFile = Header.insertFile(
                fs,
                name = file.name,
                directory = file.directory,
                contentSize = lengthToWrite,
                maxContentSize = lengthToWrite
            )
            offsetContent = newFile.offset
        }

        RandomAccessFile(fs, ACCESS_MODE)
            .also {
                it.seek(offsetContent)
                it.writeBytes(file.directory)
                it.writeBytes(file.name)
                it.writeBytes(content)
                it.close()
            }
    }

    @Throws(MalformedQueryException::class)
    private fun verifyFileSystemIntegrity() {
        if (fs.readLines().first() != Header.FILE_IDENTIFIER) throw MalformedQueryException()
    }

    @Throws(IllegalFileNameException::class)
    private fun String.checkFileNameOrThrow() {
        if (this.isBlank()) throw IllegalFileNameException("File name is empty")
        if (this.length > MAX_FILE_NAME_LENGTH) throw IllegalFileNameException("File name is too long")
    }

    @Throws(IllegalDirectoryNameException::class)
    private fun String.cleanDirectoryName(): String {
        if (this.contains("//")) throw IllegalDirectoryNameException(this)
        if (this.length > MAX_DIR_NAME_LENGTH) throw IllegalDirectoryNameException("Directory name is too long")
        return trim().removePrefix(prefix = PATH_SEPARATOR).removeSuffix(suffix = PATH_SEPARATOR)
    }
}