package com.test.filesytem

import com.test.filesytem.NodeFactory.OFFSET_PADDING_CHAR
import com.test.filesytem.NodeFactory.padOffset
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.Node
import com.test.filesytem.data.Node.Companion.ITEM_SEPARATOR
import java.io.File
import java.io.RandomAccessFile

internal object NodeFinder {

    /**
     * Returns a `Node.File` if parameters match an entry in the file system, otherwise returns null.
     */
    fun findFile(fs: File, name: String, directory: String): Node.File? {
        val directoryHashcode = directory.hashCode().toString()
        val nameHashcode = name.hashCode().toString()
        val fileNodeQuery = listOf(
            Node.FILE_IDENTIFIER,
            directoryHashcode.padOffset(),
            nameHashcode.padOffset(),
            directory.length.toString().padOffset(),
            name.length.toString().padOffset()
        ).joinToString(separator = ITEM_SEPARATOR)

        var nodeString: String? = null
        fs.readFSHeader { _, line ->
            if (line.startsWith(fileNodeQuery)) {
                nodeString = line
                return@readFSHeader true
            }
            return@readFSHeader false
        }
        return NodeFactory.createNodeFromEntry(query = nodeString) as? Node.File
    }

    /**
     * Returns a `Node.File` if parameters match an entry in the file system, otherwise returns null.
     */
    fun findDirectory(fs: File, directory: String): Node.Directory? {
        val directoryHashcode = directory.hashCode().toString()
        val directoryNodeQuery = listOf(
            Node.DIRECTORY_IDENTIFIER,
            directoryHashcode.padOffset(),
            directory.length.toString().padOffset(),
        ).joinToString(separator = ITEM_SEPARATOR)

        var nodeString: String? = null
        fs.readFSHeader { _, line ->
            if (line.startsWith(directoryNodeQuery)) {
                nodeString = line
                return@readFSHeader true
            }
            return@readFSHeader false
        }
        return NodeFactory.createNodeFromEntry(query = nodeString) as? Node.Directory
    }

    /**
     * Returns the last `Node.NextHeader`.
     */
    fun findLastNextHeader(fs: File): Node.NextHeader? {
        val nextHeaderNodeQuery = listOf(
            Node.NEXT_HEADER_IDENTIFIER,
            OFFSET_PADDING_CHAR.toString().padOffset(),
        ).joinToString(separator = ITEM_SEPARATOR)

        var nodeString: String? = null
        fs.readFSHeader { _, line ->
            if (line.startsWith(nextHeaderNodeQuery)) {
                nodeString = line
                return@readFSHeader true
            }
            return@readFSHeader false
        }
        return NodeFactory.createNodeFromEntry(query = nodeString) as? Node.NextHeader
    }

    /**
     * Returns all subdirectories for a given name.
     */
    fun findSubDirectoriesInDirectory(fs: File, directory: String): List<Node.Directory> {
        val directorySuffixed = directory + Node.PATH_SEPARATOR
        val subDirectories = mutableListOf<Node.Directory>()
        fs.readFSHeader { _, line ->
            val node = NodeFactory.createNodeFromEntry(query = line)
            if (node is Node.Directory) {
                val directoryName = fs.read(offset = node.offset, length = node.nameLength)
                if (directoryName.startsWith(directorySuffixed))
                    subDirectories.add(node)
            }
            return@readFSHeader false
        }
        return subDirectories
    }

    /**
     * Returns all files in the given directory name and its subdirectories.
     */
    fun findFilesInDirectory(fs: File, directory: String): List<Node.File> {
        val directorySuffixed = directory + Node.PATH_SEPARATOR
        val subDirectories = mutableListOf<Node.File>()
        fs.readFSHeader { _, line ->
            val node = NodeFactory.createNodeFromEntry(query = line)
            if (node is Node.File) {
                val directoryName = fs.read(offset = node.offset, length = node.directoryLength) + Node.PATH_SEPARATOR
                if (directoryName.startsWith(directorySuffixed))
                    subDirectories.add(node)
            }
            return@readFSHeader false
        }
        return subDirectories
    }

    /**
     * Find the given old node to replace to the new one. No new entry are created in the header.
     */
    fun replaceNode(fs: File, old: Node, new: Node) {
        var offsetNode = 0L
        val oldNodeEntry = NodeFactory.createNodeEntry(old)
        fs.readFSHeader { offset, line ->
            if (line == oldNodeEntry) {
                offsetNode = offset
                return@readFSHeader true
            }
            return@readFSHeader false
        }
        if (offsetNode == 0L) throw MalformedQueryException()
        RandomAccessFile(fs, SimpleFileSystem.ACCESS_MODE)
            .also {
                it.seek(offsetNode)
                it.writeBytes(NodeFactory.createNodeEntry(new))
                it.close()
            }
    }
}