package com.test.filesytem

import com.test.filesytem.Header.LINE_SEPARATOR
import com.test.filesytem.Header.LINE_SEPARATOR_LENGTH
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.NoSuchFileException
import com.test.filesytem.data.Node
import com.test.filesytem.data.SimpleFile
import java.io.File
import java.io.InputStream
import java.io.RandomAccessFile
import java.lang.Integer.max

internal object Header {

    const val FILE_IDENTIFIER = "SIMPLE_FILE_SYSTEM_001"

    val LINE_SEPARATOR: String = System.lineSeparator()
    val LINE_SEPARATOR_LENGTH = LINE_SEPARATOR.length

    private const val PRE_CONTENT_SIZE = 10
    private const val PRE_CONTENT = "*"

    // Can be updated to have bigger header size, then insert less new headers.
    private const val DEFAULT_ITEM_NUMBER = 6

    fun initFSHeader(fs: File) {
        fs.writeLine(FILE_IDENTIFIER)
        addNewHeader(fs, isFirstHeader = true)
    }

    /**
     * Insert a new file node in the file system header, if necessary add a new header.
     * Returns the created `Node.File`.
     * @param `contentSize` is used to set the content length of the given size. If superior to 0,
     * it means a write action of that length is expected on that file.
     * @param `maxContentSize` is used to set the maximum content size for that file. Should be higher than `contentSize`.
     */
    fun insertFile(fs: File, name: String, directory: String, contentSize: Int = 0, maxContentSize: Int = PRE_CONTENT_SIZE): Node.File {
        val contentToWrite = name.length + directory.length + max(contentSize, maxContentSize)
        if (!hasFreeSpace(fs, contentToWrite)) {
            addNewHeader(fs, isFirstHeader = false)
        }
        val (offsetHeader, offsetContent) = getOffsetHeaderAndContent(fs, contentToWrite)
        val newNode = NodeFactory.createFileNode(name, directory, offsetContent.toInt(), contentSize, maxContentSize)
        RandomAccessFile(fs, SimpleFileSystem.ACCESS_MODE)
            .also {
                // Header
                it.seek(offsetHeader)
                it.writeBytes(NodeFactory.createNodeEntry(newNode))

                // Content
                it.seek(offsetContent)
                it.writeBytes(directory)
                it.writeBytes(name)
                it.writeBytes(PRE_CONTENT.repeat(PRE_CONTENT_SIZE))

                it.close()
            }
        return newNode
    }

    /**
     * Insert a new directory node in the file system header, if necessary add a new header.
     * Returns the created `Node.Directory`.
     */
    fun insertDirectory(fs: File, directory: String): Node.Directory {
        val contentToWrite = directory.length
        if (!hasFreeSpace(fs, contentToWrite)) {
            addNewHeader(fs, isFirstHeader = false)
        }
        val (offsetHeader, offsetContent) = getOffsetHeaderAndContent(fs, contentToWrite)
        val newNode = NodeFactory.createDirectoryNode(directory, offsetContent.toInt())
        RandomAccessFile(fs, SimpleFileSystem.ACCESS_MODE)
            .also {
                // Header
                it.seek(offsetHeader)
                it.writeBytes(NodeFactory.createNodeEntry(newNode))

                // Content
                it.seek(offsetContent)
                it.writeBytes(directory)

                it.close()
            }
        return newNode
    }

    /**
     * Remove the file node and replace it by a `Node.EmptyBlock`.
     * File content is not deleted or overwritten, but can be reused later if replace this empty block.
     */
    @Throws(NoSuchFileException::class, MalformedQueryException::class)
    fun removeFileNode(fs: File, fileNode: Node.File) {
        val emptyNode = Node.EmptyBlock(offset = fileNode.offset, length = fileNode.totalLength)
        NodeFinder.replaceNode(fs, fileNode, emptyNode)
    }

    /**
     * Remove the directory node and replace it by a `Node.EmptyBlock`.
     * Directory name is not deleted or overwritten, but can be reused later if replace this empty block.
     */
    @Throws(NoSuchFileException::class, MalformedQueryException::class)
    fun removeDirectoryNode(fs: File, directoryNode: Node.Directory) {
        val emptyNode = Node.EmptyBlock(offset = directoryNode.offset, length = directoryNode.nameLength)
        NodeFinder.replaceNode(fs, directoryNode, emptyNode)
    }

    /**
     * Check if headers contain any entry to fit at least the @param minimumSpace.
     */
    private fun hasFreeSpace(fs: File, minimumSpace: Int): Boolean {
        var spaceFound = false
        fs.readFSHeader { _, line ->
            val node = NodeFactory.createNodeFromEntry(line)
            if (node is Node.Unassigned || node is Node.EmptyBlock && node.length >= minimumSpace) {
                spaceFound = true
                return@readFSHeader true
            }
            return@readFSHeader false
        }
        return spaceFound
    }

    /**
     * Insert a new header at the end of the file.
     * Update the previous entry `Node.NextHeader` to point to this new one.
     */
    private fun addNewHeader(fs: File, isFirstHeader: Boolean) {
        if (!isFirstHeader) {
            val offset = fs.length() + LINE_SEPARATOR_LENGTH
            val nextHeaderNode = NodeFinder.findLastNextHeader(fs) ?: throw MalformedQueryException()
            NodeFinder.replaceNode(fs, nextHeaderNode, nextHeaderNode.copy(offset = offset))
            fs.writeLine("")
        }
        val unassignedNode = NodeFactory.createNodeEntry(Node.Unassigned)
        (0..DEFAULT_ITEM_NUMBER).forEach { _ -> fs.writeLine(unassignedNode) }
        fs.writeLine(NodeFactory.createNodeEntry(Node.NextHeader(offset = 0)))
    }

    /**
     * Search for an unassigned or empty entry and return its offset header alongside with the offset to write content.
     */
    private fun getOffsetHeaderAndContent(fs: File, contentToWrite: Int): Pair<Long, Long> {
        var offsetHeader = 0L
        var offsetContent = 0L
        fs.readFSHeader { offset, line ->
            offsetHeader = offset
            val node = NodeFactory.createNodeFromEntry(line)
            if (node is Node.Unassigned) {
                offsetContent = fs.length()
                return@readFSHeader true
            }
            if (node is Node.EmptyBlock && node.length >= contentToWrite) {
                offsetContent = node.offset
                return@readFSHeader true
            }
            return@readFSHeader false
        }
        if (offsetHeader == 0L || offsetContent == 0L) throw MalformedQueryException()
        return offsetHeader to offsetContent
    }
}

private fun File.writeLine(content: String) {
    appendText(content + LINE_SEPARATOR)
}

internal fun File.read(offset: Long, length: Long): String {
    return RandomAccessFile(this, SimpleFileSystem.ACCESS_MODE)
        .let { raf ->
            raf.seek(offset)
            val bytes = ByteArray(length.toInt())
            raf.read(bytes, 0, length.toInt())
            return@let bytes.decodeToString()
        }
}

internal fun File.readFSHeader(action: (Long, String) -> Boolean) {
    val inputStream: InputStream = inputStream()
    val bufferReader = inputStream.bufferedReader()
    var offset = 0L
    RandomAccessFile(this, SimpleFileSystem.ACCESS_MODE)
        .also { randomAccessFile ->
            var line: String? = randomAccessFile.readLine()
            while (line != null) {
                val node = NodeFactory.createNodeFromEntry(line)
                if (action(offset, line)) return@also
                when {
                    node is Node.NextHeader && node.offset <= 0 -> return@also
                    node is Node.NextHeader && node.offset > 0 -> {
                        offset = node.offset
                        randomAccessFile.seek(offset)
                    }
                    else -> {
                        offset += line.length + LINE_SEPARATOR_LENGTH
                    }
                }
                line = randomAccessFile.readLine()
            }
            randomAccessFile.close()
        }
    bufferReader.close()
}
