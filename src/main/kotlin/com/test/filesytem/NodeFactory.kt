package com.test.filesytem

import com.test.filesytem.Header.FILE_IDENTIFIER
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.Node
import com.test.filesytem.data.Node.Companion.ITEM_SEPARATOR

internal object NodeFactory {

    const val OFFSET_PADDING_CHAR = '0'
    private const val OFFSET_LENGTH = Int.MAX_VALUE.toString().length + 1
    private val OFFSET_PADDING = OFFSET_PADDING_CHAR.toString().repeat(OFFSET_LENGTH)
    private const val NUMBER_COLUMNS_HEADER_ENTRY = 8

    /**
     * Parse @param query and create the corresponding `Node`.
     */
    @Throws(MalformedQueryException::class)
    fun createNodeFromEntry(query: String?): Node? {
        if (query == null) return null
        else if (query == FILE_IDENTIFIER) return null
        val items = query.split(ITEM_SEPARATOR)
        val prefix = OFFSET_PADDING_CHAR.toString()
        try {
            return when (items[0]) {
                Node.DIRECTORY_IDENTIFIER -> Node.Directory(
                    hashcode = items[1],
                    nameLength = items[2].removePrefix(prefix).toLong(),
                    offset = items[3].removePrefix(prefix).toLong(),
                )
                Node.FILE_IDENTIFIER -> Node.File(
                    hashcodeDirectory = items[1],
                    hashcodeFilename = items[2],
                    directoryLength = items[3].removePrefix(prefix).toLong(),
                    filenameLength = items[4].removePrefix(prefix).toLong(),
                    offset = items[5].removePrefix(prefix).toLong(),
                    contentLength =  items[6].removePrefix(prefix).toLong(),
                    maxContentLength = items[7].removePrefix(prefix).toLong()
                )
                Node.EMPTY_BLOCK_IDENTIFIER -> Node.EmptyBlock(
                    offset = items[1].removePrefix(prefix).toLong(),
                    length = items[2].removePrefix(prefix).toLong()
                )
                Node.UNASSIGNED_IDENTIFIER -> Node.Unassigned
                Node.NEXT_HEADER_IDENTIFIER -> Node.NextHeader(offset = items[1].removePrefix(prefix).toLong())
                else -> throw MalformedQueryException(query)
            }
        }
        catch (e: Exception) {
            throw MalformedQueryException(e.message)
        }
    }

    /**
     * From a `Node` returns a `String` corresponding to the entry to insert in the Header.
     * All generated String are the same length, and padded if necessary.
     */
    fun createNodeEntry(node: Node): String {
        val items = mutableListOf<String>()
        items.add(node.getIdentifier())
        when (node) {
            is Node.Directory -> {
                items.add(node.hashcode.padOffset())
                items.add(node.nameLength.toString().padOffset())
                items.add(node.offset.toString().padOffset())
            }
            is Node.File -> {
                items.add(node.hashcodeDirectory.padOffset())
                items.add(node.hashcodeFilename.padOffset())
                items.add(node.directoryLength.toString().padOffset())
                items.add(node.filenameLength.toString().padOffset())
                items.add(node.offset.toString().padOffset())
                items.add(node.contentLength.toString().padOffset())
                items.add(node.maxContentLength.toString().padOffset())
            }
            is Node.EmptyBlock -> {
                items.add(node.offset.toString().padOffset())
                items.add(node.length.toString().padOffset())
            }
            is Node.NextHeader -> items.add(node.offset.toString().padOffset())
            Node.Unassigned -> {}
        }

        while (items.size < NUMBER_COLUMNS_HEADER_ENTRY) {
            items.add(OFFSET_PADDING)  // Add extra space so all entries are the same size
        }
        return items.joinToString(ITEM_SEPARATOR)
    }

    /**
     * Creates `Node.File` from parameters.
     */
    fun createFileNode(name: String, directory: String, offset: Int, contentLength: Int, maxContentSize: Int): Node.File {
        return Node.File(
            hashcodeDirectory = directory.hashCode().toString(),
            hashcodeFilename = name.hashCode().toString(),
            directoryLength = directory.length.toLong(),
            filenameLength = name.length.toLong(),
            offset = offset.toLong(),
            contentLength = contentLength.toLong(),
            maxContentLength = maxContentSize.toLong()
        )
    }

    /**
     * Creates `Node.Directory` from parameters.
     */
    fun createDirectoryNode(directory: String, offset: Int): Node.Directory {
        return Node.Directory(
            hashcode = directory.hashCode().toString(),
            nameLength = directory.length.toLong(),
            offset = offset.toLong(),
        )
    }

    internal fun String.padOffset(): String = this.padStart(length = OFFSET_LENGTH, padChar = OFFSET_PADDING_CHAR)
}