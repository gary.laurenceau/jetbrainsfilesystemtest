package com.test.filesytem.data

data class SimpleFile(val name: String, val directory: String)
data class SimpleDirectory(val path: String)
