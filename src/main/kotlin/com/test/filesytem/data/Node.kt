package com.test.filesytem.data

internal sealed class Node {

    data class EmptyBlock(
        val offset: Long,
        val length: Long
    ): Node()

    data class File(
        val hashcodeDirectory: String,
        val hashcodeFilename: String,
        val directoryLength: Long,
        val filenameLength: Long,
        val offset: Long,
        val contentLength: Long,
        val maxContentLength: Long
    ): Node() {
        val offsetContent = offset + directoryLength + filenameLength
        val totalLength = directoryLength + filenameLength + maxContentLength
    }

    data class Directory(
        val hashcode: String,
        val nameLength: Long,
        val offset: Long,
    ) : Node()

    object Unassigned: Node()
    data class NextHeader(val offset: Long): Node()

    fun getIdentifier(): String {
        return when (this) {
            is EmptyBlock -> EMPTY_BLOCK_IDENTIFIER
            is File -> FILE_IDENTIFIER
            is Directory -> DIRECTORY_IDENTIFIER
            is NextHeader -> NEXT_HEADER_IDENTIFIER
            Unassigned -> UNASSIGNED_IDENTIFIER
        }
    }

    companion object {
        const val PATH_SEPARATOR = "/"
        const val EMPTY_BLOCK_IDENTIFIER = "e"
        const val FILE_IDENTIFIER = "f"
        const val DIRECTORY_IDENTIFIER = "d"
        const val UNASSIGNED_IDENTIFIER = "u"
        const val NEXT_HEADER_IDENTIFIER = "n"
        const val ITEM_SEPARATOR = ":"
    }
}

