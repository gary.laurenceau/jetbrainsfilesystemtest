package com.test.filesytem.data

import java.io.IOException

/**
 * An exception class which thrown when File System can't be initialized (permission issue, not enough space, etc).
 */
object InitializeFileSystemException: Throwable("Initialize File System exception")
/**
 * An exception class which thrown when parsing the file system and unknown item is found.
 */
class MalformedQueryException(reason: String? = null): Throwable(reason)

/**
 * An exception class which is used when file to create already exists.
 */
class FileAlreadyExistsException(filename: String? = null) : IOException(filename)
/**
 * An exception class which is used when the filename is invalid.
 */
class IllegalFileNameException(filename: String): Throwable(filename)
/**
 * An exception class which is used when specified file can't be found in the file system.
 */
class NoSuchFileException(filename: String? = null) : IOException("File '$filename' not found")

/**
 * An exception class which is used when directory to create already exists.
 */
class DirectoryAlreadyExistsException(directory: String? = null) : IOException(directory)
/**
 * An exception class which is used when the directory name is empty, invalid or equivalent to root.
 */
class IllegalDirectoryNameException(directory: String? = null) : IOException(directory)
/**
 * An exception class which is used when specified directory can't be found in the file system.
 */
class NoSuchDirectoryException(directory: String? = null) : IOException("Directory '$directory' not found")
