package com.test.filesytem

import com.test.filesytem.data.FileAlreadyExistsException
import com.test.filesytem.data.SimpleDirectory
import com.test.filesytem.data.SimpleFile

interface FileSystem {

    @Throws(FileAlreadyExistsException::class)
    fun newFile(name: String, directory: String = ""): SimpleFile
    fun newDirectory(path: String): SimpleDirectory

    fun deleteFile(file: SimpleFile)
    fun deleteDirectory(directory: SimpleDirectory)

    fun readFile(file: SimpleFile): String
    fun writeFile(file: SimpleFile, content: String)

}