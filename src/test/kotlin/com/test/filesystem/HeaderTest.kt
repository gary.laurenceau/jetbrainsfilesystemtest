package com.test.filesystem

import com.test.filesytem.Header
import com.test.filesytem.NodeFactory
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.BeforeTest
import kotlin.test.assertEquals

object HeaderTest {

    private lateinit var fs: File

    @BeforeTest
    fun setup() {
        fs = File("test.fs")
        if (fs.exists()) fs.delete()
    }

    @Test
    fun `Initialize file system`() {
        Header.initFSHeader(fs)

        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }

        assertEquals(
            """
                SIMPLE_FILE_SYSTEM_001
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                
            """.trimIndent(),
            inputString
        )
    }

    // Create file

    @Test
    fun `Insert file, replace a unassigned node`() {
        val defaultContent = """
                SIMPLE_FILE_SYSTEM_001
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                
            """.trimIndent()
        fs.writeText(defaultContent)
        val filename = "file.txt"
        val directory = ""
        val fileNode = NodeFactory.createFileNode(
            name = filename,
            directory = directory,
            offset = defaultContent.length, // Correspond to the offset after the first header
            contentLength = 0,
            maxContentSize = 10
        )

        Header.insertFile(fs, name = filename, directory = directory, contentSize = 0, maxContentSize = 10)

        val fileNodeEntry = NodeFactory.createNodeEntry(fileNode)
        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        assertEquals(
            """
                SIMPLE_FILE_SYSTEM_001
                $fileNodeEntry
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                $filename**********
            """.trimIndent(),
            inputString
        )
    }

    @Test
    fun `Insert file, replace an empty node`() {
        val defaultContent = """
                SIMPLE_FILE_SYSTEM_001
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                e:00000000380:00000000020:00000000000:00000000000:00000000000:00000000000:00000000000
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                FIRST_FILE***EMPTY_BLOCK+++++++++ANOTHER_FILE_CONTENT
            """.trimIndent()
        fs.writeText(defaultContent)
        val filename = "file.txt"
        val directory = ""
        val fileNode = NodeFactory.createFileNode(
            name = filename,
            directory = directory,
            offset = 380, // Correspond to the offset given by the empty block node
            contentLength = 0,
            maxContentSize = 10
        )

        Header.insertFile(fs, name = filename, directory = directory, contentSize = 0, maxContentSize = 10)

        val fileNodeEntry = NodeFactory.createNodeEntry(fileNode)
        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        assertEquals(
            """
                SIMPLE_FILE_SYSTEM_001
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                $fileNodeEntry
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                FIRST_FILE***file.txt**********++ANOTHER_FILE_CONTENT
            """.trimIndent(),
            inputString
        )
    }

    @Test
    fun `Insert file but create a new header for more entries`() {
        val defaultContent = """
                SIMPLE_FILE_SYSTEM_001
                e:00000000281:00000000010:00000000000:00000000000:00000000000:00000000000:00000000000
                e:00000000291:00000000010:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                EMPTY_DATAEMPTY_DATA
            """.trimIndent()
        fs.writeText(defaultContent)
        val filename = "file.txt"
        val directory = ""
        val fileNode = NodeFactory.createFileNode(
            name = filename,
            directory = directory,
            offset = 990, // Correspond to the offset after the last header.
            contentLength = 0,
            maxContentSize = 50
        )

        Header.insertFile(fs, name = filename, directory = directory, contentSize = 0, maxContentSize = 50)

        val fileNodeEntry = NodeFactory.createNodeEntry(fileNode)
        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        assertEquals(
            """
                SIMPLE_FILE_SYSTEM_001
                e:00000000281:00000000010:00000000000:00000000000:00000000000:00000000000:00000000000
                e:00000000291:00000000010:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000302:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                EMPTY_DATAEMPTY_DATA
                $fileNodeEntry
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                $filename**********
            """.trimIndent(),
            inputString
        )
    }

    // Create directory entries

    @Test
    fun `Insert directory, replace a unassigned node`() {
        val defaultContent = """
                SIMPLE_FILE_SYSTEM_001
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                
            """.trimIndent()
        fs.writeText(defaultContent)
        val directory = "folder"
        val directoryNode = NodeFactory.createDirectoryNode(
            directory = directory,
            offset = defaultContent.length, // Correspond to the offset after the first header
        )

        Header.insertDirectory(fs, directory = directory)

        val directoryNodeEntry = NodeFactory.createNodeEntry(directoryNode)
        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        assertEquals(
            """
                SIMPLE_FILE_SYSTEM_001
                $directoryNodeEntry
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                $directory
            """.trimIndent(),
            inputString
        )
    }

    @Test
    fun `Insert directory but create a new header for more entries`() {
        val defaultContent = """
                SIMPLE_FILE_SYSTEM_001
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                USED_DATA
            """.trimIndent()
        fs.writeText(defaultContent)
        val directory = "anotherFolder"
        val directoryNode = NodeFactory.createDirectoryNode(
            directory = directory,
            offset = 979, // Correspond to the offset after the last header.
        )

        Header.insertDirectory(fs, directory = directory)

        val directoryNodeEntry = NodeFactory.createNodeEntry(directoryNode)
        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        assertEquals(
            """
                SIMPLE_FILE_SYSTEM_001
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                f:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000291:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                USED_DATA
                $directoryNodeEntry
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
                anotherFolder
            """.trimIndent(),
            inputString
        )
    }
}