package com.test.filesystem

import com.test.filesytem.NodeFactory
import com.test.filesytem.NodeFactory.padOffset
import com.test.filesytem.NodeFinder
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.Node
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.File
import kotlin.test.BeforeTest
import kotlin.test.assertEquals
import kotlin.test.assertTrue

object NodeFinderTest {

    private lateinit var fs: File

    @BeforeTest
    fun setup() {
        fs = File("test.fs")
        if (fs.exists()) fs.delete()
    }

    // Find files

    @Test
    fun `Find file in FS but it doesn't exist`() {
        // Contains
        // File `filename` in root
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            f:00000000000:-1224674847:00000000000:00000000009:00000000639:00000000000:00000000010
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000
            test1.txt**********
        """.trimIndent())

        val fileNode = NodeFinder.findFile(fs, "filename", "")

        assertEquals(null, fileNode)
    }

    @Test
    fun `Find file in FS in first header (no header jump)`() {
        val directory = ""
        val directoryLength = directory.length.toString().padOffset()
        val hashcodeDirectory = directory.hashCode().toString().padOffset()
        val filename = "filename"
        val filenameLength = filename.length.toString().padOffset()
        val hashcodeFile = "filename".hashCode().toString().padOffset()
        val offset = 300L
        val offsetPadded = offset.toString().padOffset()
        val contentLength = 0L
        val contentLengthPadded = contentLength.toString().padOffset()
        val maxContentLength = 10L
        val maxContentLengthPadded = maxContentLength.toString().padOffset()

        // Contains
        // Unassigned node
        // File `filename`
        // Unassigned node
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            f:$hashcodeDirectory:$hashcodeFile:$directoryLength:$filenameLength:$offsetPadded:$contentLengthPadded:$maxContentLengthPadded
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000
            $directory$filename**********
        """.trimIndent())

        val fileNode = NodeFinder.findFile(fs, filename, directory)

        val expectedFileNode = Node.File(
            hashcodeDirectory = hashcodeDirectory,
            hashcodeFilename = hashcodeFile,
            directoryLength = directory.length.toLong(),
            filenameLength = filenameLength.toLong(),
            offset = offset,
            contentLength = contentLength,
            maxContentLength = maxContentLength
        )
        assertEquals(expectedFileNode, fileNode)
    }

    @Test
    fun `Find file in FS in the next header header (with header jump)`() {
        val directory = ""
        val directoryLength = directory.length.toString().padOffset()
        val hashcodeDirectory = directory.hashCode().toString().padOffset()
        val filename = "filename"
        val filenameLength = filename.length.toString().padOffset()
        val hashcodeFile = "filename".hashCode().toString().padOffset()
        val offset = 400L
        val offsetPadded = offset.toString().padOffset()
        val contentLength = 0L
        val contentLengthPadded = contentLength.toString().padOffset()
        val maxContentLength = 10L
        val maxContentLengthPadded = maxContentLength.toString().padOffset()

        // Contains some random files in first header, and one file called `filename` in the second header
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            f:00000000000:-1224674847:00000000000:00000000009:00000000813:00000000012:00000000012
            f:00000000000:01577563260:00000000000:00000000017:00000001451:00000000546:00000000546
            n:00000000406:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            test1.txt**********test2.txt**********
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            f:$hashcodeDirectory:$hashcodeFile:$directoryLength:$filenameLength:$offsetPadded:$contentLengthPadded:$maxContentLengthPadded
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            $directory$filename**********
        """.trimIndent())

        val fileNode = NodeFinder.findFile(fs, filename, directory)

        val expectedFileNode = Node.File(
            hashcodeDirectory = hashcodeDirectory,
            hashcodeFilename = hashcodeFile,
            directoryLength = directory.length.toLong(),
            filenameLength = filenameLength.toLong(),
            offset = offset,
            contentLength = contentLength,
            maxContentLength = maxContentLength
        )
        assertEquals(expectedFileNode, fileNode)
    }
    @Test
    fun `Find file with same name but in different directories`() {
        val directory1 = "directory1"
        val directory1Length = directory1.length.toString().padOffset()
        val hashcodeDirectory1 = directory1.hashCode().toString().padOffset()

        val directory2 = "directory10"
        val directory2Length = directory2.length.toString().padOffset()
        val hashcodeDirectory2 = directory2.hashCode().toString().padOffset()

        val filename = "filename"
        val filenameLength = filename.length.toString().padOffset()
        val hashcodeFile = "filename".hashCode().toString().padOffset()
        val offset = 400L
        val offsetPadded = offset.toString().padOffset()
        val contentLength = 0L
        val contentLengthPadded = contentLength.toString().padOffset()
        val maxContentLength = 10L
        val maxContentLengthPadded = maxContentLength.toString().padOffset()

        // Contains 2 files called `filename` in two different directories.
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            f:00000000000:-1224674847:00000000000:00000000009:00000000813:00000000012:00000000012
            f:$hashcodeDirectory2:$hashcodeFile:$directory2Length:$filenameLength:$offsetPadded:$contentLengthPadded:$maxContentLengthPadded
            n:00000000397:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            $directory2$filename**********
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            f:$hashcodeDirectory1:$hashcodeFile:$directory1Length:$filenameLength:$offsetPadded:$contentLengthPadded:$maxContentLengthPadded
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            $directory1$filename**********
        """.trimIndent())

        fs.readLines().forEach { println(it) }

        val fileNode1 = NodeFinder.findFile(fs, filename, directory1)
        val fileNode2 = NodeFinder.findFile(fs, filename, directory2)

        val expectedFileNode1 = Node.File(
            hashcodeDirectory = hashcodeDirectory1,
            hashcodeFilename = hashcodeFile,
            directoryLength = directory1.length.toLong(),
            filenameLength = filenameLength.toLong(),
            offset = offset,
            contentLength = contentLength,
            maxContentLength = maxContentLength
        )
        val expectedFileNode2 = Node.File(
            hashcodeDirectory = hashcodeDirectory2,
            hashcodeFilename = hashcodeFile,
            directoryLength = directory2.length.toLong(),
            filenameLength = filenameLength.toLong(),
            offset = offset,
            contentLength = contentLength,
            maxContentLength = maxContentLength
        )
        assertEquals(expectedFileNode1, fileNode1)
        assertEquals(expectedFileNode2, fileNode2)
        assertEquals(hashcodeDirectory1, fileNode1!!.hashcodeDirectory)
        assertEquals(hashcodeDirectory2, fileNode2!!.hashcodeDirectory)
    }

    // Find directory

    @Test
    fun `Find directory in FS but it doesn't exist`() {
        // Contains only a directory named `directory1`
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            d:-1224674847:00000000010:00000000367:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            directory1**********
        """.trimIndent())

        val directoryNode = NodeFinder.findDirectory(fs, directory = "dir")

        assertEquals(null, directoryNode)
    }

    @Test
    fun `Find directory in FS`() {
        val directory = "new-folder"
        val directoryLength = directory.length.toString().padOffset()
        val hashcodeDirectory = directory.hashCode().toString().padOffset()
        val offset = 300L
        val offsetPadded = offset.toString().padOffset()

        // Contains one directory
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            d:$hashcodeDirectory:$directoryLength:$offsetPadded:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            $directory
        """.trimIndent())

        val directoryNode = NodeFinder.findDirectory(fs, directory)

        val expectedDirectoryNode = Node.Directory(
            hashcode = hashcodeDirectory,
            nameLength = directory.length.toLong(),
            offset = offset
        )
        assertEquals(expectedDirectoryNode, directoryNode)
    }

    // Next header

    @Test
    fun `Find last header, but only one exists`() {
        val offset = 0L
        val offsetPadded = offset.toString().padOffset()

        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:$offsetPadded:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
        """.trimIndent())

        val lastHeaderNode = NodeFinder.findLastNextHeader(fs)

        val expectedNextHeaderNode = Node.NextHeader(offset = offset)
        assertEquals(expectedNextHeaderNode, lastHeaderNode)
    }

    @Test
    fun `Find last header`() {
        val firstOffset = 367L
        val firstOffsetPadded = firstOffset.toString().padOffset()
        val secondOffset = 0L
        val secondOffsetPadded = secondOffset.toString().padOffset()

        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:$firstOffsetPadded:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:$secondOffsetPadded:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
        """.trimIndent())

        val lastHeaderNode = NodeFinder.findLastNextHeader(fs)

        val expectedNextHeaderNode = Node.NextHeader(offset = secondOffset)
        assertEquals(expectedNextHeaderNode, lastHeaderNode)
    }

    // Find subdirectories

    @Test
    fun `Find subdirectories for a given directory name`() {
        // Contains:
        // DIR_HASH1: folder
        // DIR_HASH2: sub
        // DIR_HASH3: sub/dir
        // DIR_HASH4: sub/dir/another
        // DIR_HASH5: folder2
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            d:00DIR_HASH1:00000000006:00000000367:00000000000:00000000000:00000000000:00000000000
            d:00DIR_HASH2:00000000003:00000000373:00000000000:00000000000:00000000000:00000000000
            d:00DIR_HASH3:00000000007:00000000376:00000000000:00000000000:00000000000:00000000000
            n:00000000384:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            foldersubsub/dir
            SIMPLE_FILE_SYSTEM_001
            d:00DIR_HASH4:00000000015:00000000751:00000000000:00000000000:00000000000:00000000000
            d:00DIR_HASH5:00000000007:00000000766:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            sub/dir/anotherfolder2
        """.trimIndent())

        val subdirectories = NodeFinder.findSubDirectoriesInDirectory(fs, "sub")
        assertEquals(2, subdirectories.size)
        assertTrue { subdirectories.map { it.hashcode }.contains("00DIR_HASH3") }
        assertTrue { subdirectories.map { it.hashcode }.contains("00DIR_HASH4") }
    }

    @Test
    fun `No subdirectories found for a given directory name`() {
        // Contains:
        // DIR_HASH1: folder
        // DIR_HASH2: sub
        // DIR_HASH3: sub/dir
        // DIR_HASH4: sub/dir/another
        // DIR_HASH5: folder2
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            d:00DIR_HASH1:00000000006:00000000367:00000000000:00000000000:00000000000:00000000000
            d:00DIR_HASH2:00000000003:00000000373:00000000000:00000000000:00000000000:00000000000
            d:00DIR_HASH3:00000000007:00000000376:00000000000:00000000000:00000000000:00000000000
            n:00000000384:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            foldersubsub/dir
            SIMPLE_FILE_SYSTEM_001
            d:00DIR_HASH4:00000000015:00000000751:00000000000:00000000000:00000000000:00000000000
            d:00DIR_HASH5:00000000007:00000000766:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            sub/dir/anotherfolder2
        """.trimIndent())

        val subdirectories = NodeFinder.findSubDirectoriesInDirectory(fs, "folder")

        assertEquals(0, subdirectories.size)
    }

    // Find files in subdirectories for a given directory

    @Test
    fun `Find files for a given directory name`() {
        // Contains:
        // DIR_HASH1: directory `dir1`
        // FILE_HASH1: file `file1`
        // FILE_HASH2: file `file2` in `dir1`
        // DIR_HASH4: directory `dir1/dir2`
        // FILE_HASH3: file `file2` in `dir1/dir2`
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            d:00DIR_HASH1:00000000004:00000000367:00000000000:00000000000:00000000000:00000000000
            f:00000000000:0FILE_HASH1:00000000000:00000000005:00000000371:00000000000:00000000000
            f:00DIR_HASH1:0FILE_HASH2:00000000004:00000000005:00000000376:00000000000:00000000000
            n:00000000386:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            dir1file1dir1file2
            SIMPLE_FILE_SYSTEM_001
            d:00DIR_HASH4:00000000009:00000000753:00000000000:00000000000:00000000000:00000000000
            f:00DIR_HASH4:0FILE_HASH3:00000000009:00000000005:00000000762:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            dir1/dir2dir1/dir2file2
        """.trimIndent())

        val subdirectories = NodeFinder.findFilesInDirectory(fs, "dir1")

        subdirectories.forEach { println(it.hashcodeFilename) }
        assertEquals(2, subdirectories.size)
        assertTrue { subdirectories.map { it.hashcodeFilename }.contains("0FILE_HASH2") }
        assertTrue { subdirectories.map { it.hashcodeFilename }.contains("0FILE_HASH3") }
    }

    // Replace nodes

    @Test
    fun `Replace an empty node`() {
        val emptyNode = Node.EmptyBlock(offset = 5, length = 11)
        val directoryNode = Node.Directory("hash", 4L, offset = 5)
        fs.writeText("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            e:00000000005:00000000011:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
        """.trimIndent())

        NodeFinder.replaceNode(fs, old = emptyNode, new = directoryNode)

        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        val directoryEntry = NodeFactory.createNodeEntry(directoryNode)
        assertEquals("""
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            $directoryEntry
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
        """.trimIndent(), inputString)
    }

    @Test
    fun `Try to replace a node, but it's not found `() {
        val emptyNode = Node.EmptyBlock(offset = 5, length = 11)
        val directoryNode = Node.Directory("hash", 4L, offset = 5)
        val fsContent = """
            SIMPLE_FILE_SYSTEM_001
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            e:00000000015:00000000011:00000000000:00000000000:00000000000:00000000000:00000000000
            u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
            n:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000
        """.trimIndent()
        fs.writeText(fsContent)

        assertThrows<MalformedQueryException> { NodeFinder.replaceNode(fs, old = emptyNode, new = directoryNode) }

        val inputStream = fs.inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        // Content should not have been updated
        assertEquals(fsContent, inputString)
    }
}