package com.test.filesystem

import com.test.filesytem.SimpleFileSystem
import com.test.filesytem.SimpleFileSystem.Companion.MAX_DIR_NAME_LENGTH
import com.test.filesytem.SimpleFileSystem.Companion.MAX_FILE_NAME_LENGTH
import com.test.filesytem.data.DirectoryAlreadyExistsException
import com.test.filesytem.data.FileAlreadyExistsException
import com.test.filesytem.data.IllegalDirectoryNameException
import com.test.filesytem.data.IllegalFileNameException
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.NoSuchDirectoryException
import com.test.filesytem.data.NoSuchFileException
import com.test.filesytem.data.SimpleDirectory
import com.test.filesytem.data.SimpleFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.File
import kotlin.test.BeforeTest
import kotlin.test.assertEquals

object SimpleFileSystemTest {

    private lateinit var simpleFileSystem: SimpleFileSystem

    @BeforeTest
    fun setup() {
        // Reset file system for each test
        val file = File("test.fs")
        if (file.exists()) file.delete()
        simpleFileSystem = SimpleFileSystem(file)
    }

    @Test
    fun `Create files`() {
        // Create a file and check return value
        val test1 = simpleFileSystem.newFile("test1.txt")
        assertEquals("test1.txt", test1.name)
        assertEquals("", test1.directory)

        // Create a file and check return value
        val test2 = simpleFileSystem.newFile("test2.txt")
        assertEquals("test2.txt", test2.name)
        assertEquals("", test2.directory)

        // Throw an exception is a file already exist (same file name and in the same directory)
        assertThrows<FileAlreadyExistsException> { simpleFileSystem.newFile("test1.txt") }
        assertThrows<IllegalFileNameException> { simpleFileSystem.newFile("F".repeat(MAX_FILE_NAME_LENGTH + 1)) }
    }

    @Test
    fun `Create more file than initial header stack`() {
        // Header entry size is fixed, if more entries are needed, the library creates a new header,
        // but it should not be an issue for the user.

        // Create files
        val test1 = simpleFileSystem.newFile("test1.txt")
        val test2 = simpleFileSystem.newFile("test2.txt")
        simpleFileSystem.newFile("long-file-name-123456789123456789123456789.txt")
        simpleFileSystem.newFile("another_file.txt")
        val anotherFile = simpleFileSystem.newFile("another_file2.txt")
        simpleFileSystem.newFile("another_file3.txt")

        // Write content
        simpleFileSystem.writeFile(test1, "Hello world!")
        simpleFileSystem.writeFile(anotherFile, "RandomContent".repeat(42))
        simpleFileSystem.writeFile(test2, "Lorem")

        // Verify read content
        assertEquals("Hello world!", simpleFileSystem.readFile(test1))
        assertEquals("Lorem", simpleFileSystem.readFile(test2))
    }

    @Test
    fun `Write more (test reallocation)`() {
        val test1 = simpleFileSystem.newFile("test1.txt")
        val test2 = simpleFileSystem.newFile("test2.txt")

        val loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        simpleFileSystem.writeFile(test1, "Hello world!")
        simpleFileSystem.writeFile(test2, "Second test file.")
        simpleFileSystem.writeFile(test1, loremIpsum)

        assertEquals(loremIpsum, simpleFileSystem.readFile(test1))
        assertEquals("Second test file.", simpleFileSystem.readFile(test2))
    }

    @Test
    fun `Create and delete file`() {
        val test1 = simpleFileSystem.newFile("test1.txt")

        simpleFileSystem.writeFile(test1, "Hello world!")
        simpleFileSystem.deleteFile(test1)

        assertThrows<NoSuchFileException> { simpleFileSystem.readFile(test1) }
    }

    @Test
    fun `Create directories and files`() {
        val directory1 = simpleFileSystem.newDirectory("directory1")
        val test1 = simpleFileSystem.newFile("test1.txt")
        val test2 = simpleFileSystem.newFile("test1.txt", directory1.path)

        val content1 = "Content from file in root"
        val content2 = "Content from file in sub directory: ${directory1.path}"
        simpleFileSystem.writeFile(test1, content1)
        simpleFileSystem.writeFile(test2, content2)

        assertEquals(content1, simpleFileSystem.readFile(test1))
        assertEquals(content2, simpleFileSystem.readFile(test2))
        assertThrows<IllegalDirectoryNameException> { simpleFileSystem.newDirectory("D".repeat(MAX_DIR_NAME_LENGTH + 1)) }
    }

    @Test
    fun `Create file but directory doesn't exist`() {
        // Create a directory
        simpleFileSystem.newDirectory("directory1")

        // Make sure the rules to create a file in a directory are respected
        assertThrows<NoSuchDirectoryException> { simpleFileSystem.newFile("test1.txt", "directory10") }
        assertThrows<NoSuchDirectoryException> { simpleFileSystem.newFile("test1.txt", "directory1/another") }
        assertThrows<DirectoryAlreadyExistsException> { simpleFileSystem.newDirectory("directory1") }
        simpleFileSystem.newFile("test1.txt", "directory1")
    }

    @Test
    fun `Verify valid file and directory names`() {
        assertThrows<IllegalFileNameException> { simpleFileSystem.newFile(name = "", directory = "") }
        simpleFileSystem.newFile(name = "/", directory = "")
        simpleFileSystem.newFile(name = "filename", directory = "/")
        assertThrows<FileAlreadyExistsException> { simpleFileSystem.newFile(name = "filename", directory = "") }
        assertThrows<IllegalDirectoryNameException> { simpleFileSystem.newDirectory(path = "") }
        assertThrows<IllegalDirectoryNameException> { simpleFileSystem.newDirectory(path = "/") }
        assertThrows<IllegalDirectoryNameException> { simpleFileSystem.newDirectory(path = "//") }
        assertThrows<IllegalDirectoryNameException> { simpleFileSystem.newDirectory(path = "/directory//") }
        simpleFileSystem.newDirectory(path = "/directory/")
        assertThrows<DirectoryAlreadyExistsException> { simpleFileSystem.newDirectory(path = "directory") }
    }

    @Test
    fun `Delete directory and subDirectories`() {
        val directory = SimpleDirectory("folder")

        // Directory is not created, should throw exception
        assertThrows<NoSuchDirectoryException> { simpleFileSystem.deleteDirectory(directory = directory) }

        // Create and delete directory. Then create should work again
        simpleFileSystem.newDirectory(path = directory.path)
        simpleFileSystem.deleteDirectory(directory = directory)
        simpleFileSystem.newDirectory(path = directory.path)

        // Create subdirectories and files
        simpleFileSystem.newDirectory(path = "folder2")
        simpleFileSystem.newFile(name = "file_in_subdirectory", directory = "folder2")

        simpleFileSystem.newDirectory(path = "folder/subFolder1")
        simpleFileSystem.newDirectory(path = "folder/subFolder2")
        simpleFileSystem.newDirectory(path = "folder/subFolder1/anotherSubFolder")
        val tmpFileInSubDir = simpleFileSystem.newFile(name = "file_in_subdirectory", directory = "folder/subFolder2")
        simpleFileSystem.writeFile(file = tmpFileInSubDir, content = "This is file content in a sub directory")

        // Create main directory should delete sub content
        simpleFileSystem.deleteDirectory(directory)
        assertThrows<NoSuchDirectoryException> { simpleFileSystem.newDirectory(path = "folder/subFolder1/anotherSubFolder") }
        assertThrows<NoSuchFileException> { simpleFileSystem.readFile(file = tmpFileInSubDir) }

        // Make sure other directories are not deleted
        assertThrows<DirectoryAlreadyExistsException> { simpleFileSystem.newDirectory(path = "folder2") }
        assertThrows<FileAlreadyExistsException> { simpleFileSystem.newFile(name = "file_in_subdirectory", directory = "folder2") }
    }

    @Test
    fun `Check malformed file`() {
        val file = File("test.fs")
        file.writeText("random content")
        assertThrows<MalformedQueryException> { simpleFileSystem = SimpleFileSystem(file) }
        file.delete()
        // If file is deleted, new instance of SimpleFileSystem should work
        simpleFileSystem = SimpleFileSystem(file)
    }

    @Test
    fun `Check multi thread`() {
        runBlocking {
            val scope = CoroutineScope(newFixedThreadPoolContext(nThreads = 4, name = ""))
            scope.launch {
                // Write a hundred of file in parallel
                val coroutines = 0.rangeTo(100).map { index ->
                    launch {
                        val file = simpleFileSystem.newFile("file-$index")
                        simpleFileSystem.writeFile(file, "Content: ${file.name}")
                    }
                }

                coroutines.forEach { coroutine ->
                    coroutine.join() // wait for all coroutines to finish their jobs.
                }
            }.join()
        }
        // Check all files have been created and contains their data
        0.rangeTo(100).forEach {  index ->
            assertEquals("Content: file-$index", simpleFileSystem.readFile(SimpleFile("file-$index", "")))
        }
    }
}