package com.test.filesystem

import com.test.filesytem.Header
import com.test.filesytem.NodeFactory
import com.test.filesytem.data.MalformedQueryException
import com.test.filesytem.data.Node
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

object NodeFactoryTest {

    // Create nodes

    @Test
    fun `Create node but query is invalid`() {
        assertEquals(null, NodeFactory.createNodeFromEntry(query = null))
        assertEquals(null, NodeFactory.createNodeFromEntry(query = Header.FILE_IDENTIFIER))
        assertThrows<MalformedQueryException> { NodeFactory.createNodeFromEntry(query = "") }
        assertThrows<MalformedQueryException> { NodeFactory.createNodeFromEntry(query = "a:000") }
    }

    @Test
    fun `Create file node`() {
        val fileNode = NodeFactory.createNodeFromEntry(
            query = "f:01234567890:-1234567899:000000002:00000000004:00000000008:00000000016:00000000032"
        )
        val expectedFileNode = Node.File(
            hashcodeDirectory = "01234567890",
            hashcodeFilename = "-1234567899",
            directoryLength = 2,
            filenameLength = 4,
            offset = 8,
            contentLength = 16,
            maxContentLength = 32,
        )
        assertEquals(expectedFileNode, fileNode)
    }

    @Test
    fun `Create directory node`() {
        val directoryNode = NodeFactory.createNodeFromEntry(
            query = "d:hashcodeDir:00000000008:00000000016:00000000000:00000000000:00000000000:00000000000:00000000000"
        )
        val expectedDirectoryNode = Node.Directory(
            hashcode = "hashcodeDir",
            nameLength = 8,
            offset = 16,
        )
        assertEquals(expectedDirectoryNode, directoryNode)
    }

    @Test
    fun `Create empty block node`() {
        val emptyBlockNode = NodeFactory.createNodeFromEntry(
            query = "e:00000000008:00000000016"
        )
        val expectedEmptyBlockNode = Node.EmptyBlock(
            offset = 8,
            length = 16
        )
        assertEquals(expectedEmptyBlockNode, emptyBlockNode)
    }

    @Test
    fun `Create unassigned node`() {
        val emptyBlockNode = NodeFactory.createNodeFromEntry(query = "u:00000000000:00000000000")
        assertEquals(Node.Unassigned, emptyBlockNode)
    }

    @Test
    fun `Create next header node`() {
        val nextHeaderNode = NodeFactory.createNodeFromEntry(query = "n:00000000042")
        val expectedNextHeaderNode = Node.NextHeader(offset = 42)
        assertEquals(expectedNextHeaderNode, nextHeaderNode)
    }

    // Generate nodes entry

    @Test
    fun `Generate file entry`() {
        val fileNode = Node.File(
            hashcodeDirectory = "hashcodeDir",
            hashcodeFilename = "hashcodeFile",
            directoryLength = 2,
            filenameLength = 4,
            offset = 8,
            contentLength = 16,
            maxContentLength = 32,
        )
        val entry = NodeFactory.createNodeEntry(fileNode)
        val expectedEntry = "f:hashcodeDir:hashcodeFile:00000000002:00000000004:00000000008:00000000016:00000000032"
        assertEquals(expectedEntry, entry)
    }

    @Test
    fun `Generate directory entry`() {
        val directoryNode = Node.Directory(
            hashcode = "hashcode",
            nameLength = 4,
            offset = 32
        )
        val entry = NodeFactory.createNodeEntry(directoryNode)
        val expectedEntry = "d:000hashcode:00000000004:00000000032:00000000000:00000000000:00000000000:00000000000"
        assertEquals(expectedEntry, entry)
    }

    @Test
    fun `Generate empty block entry`() {
        val emptyBlockNode = Node.EmptyBlock(offset = 8, length = 16)
        val entry = NodeFactory.createNodeEntry(emptyBlockNode)
        assertEquals("e:00000000008:00000000016:00000000000:00000000000:00000000000:00000000000:00000000000", entry)
    }

    @Test
    fun `Generate unassigned entry`() {
        val entry = NodeFactory.createNodeEntry(Node.Unassigned)
        val expectedEntry = "u:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000"
        assertEquals(expectedEntry, entry)
    }

    @Test
    fun `Generate file node entry`() {
        val nextHeaderNode = Node.NextHeader(offset = 126)
        val entry = NodeFactory.createNodeEntry(nextHeaderNode)
        assertEquals("n:00000000126:00000000000:00000000000:00000000000:00000000000:00000000000:00000000000", entry)
    }

    // Create node objects

    @Test
    fun `Create file node from parameters`() {
        val fileNode = NodeFactory.createFileNode(
            name = "filename",
            directory = "/",
            offset = 142,
            contentLength = 512,
            maxContentSize = 10000
        )
        val expectedFileNode = Node.File(
            hashcodeDirectory = "/".hashCode().toString(),
            hashcodeFilename = "filename".hashCode().toString(),
            directoryLength = 1,
            filenameLength = 8,
            offset = 142,
            contentLength = 512,
            maxContentLength = 10000
        )
        assertEquals(expectedFileNode, fileNode)
    }

    @Test
    fun `Create directory node from parameters`() {
        val directoryNode = NodeFactory.createDirectoryNode(
            directory = "dir",
            offset = 142
        )
        val expectedDirectoryNode = Node.Directory(
            hashcode = "dir".hashCode().toString(),
            nameLength = 3,
            offset = 142
        )
        assertEquals(expectedDirectoryNode, directoryNode)
    }
}